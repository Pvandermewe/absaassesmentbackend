export class PhoneBook {
    
    phoneBookId: number;
    name: string;

    constructor(dto? :any){
        if(dto) {
            this.phoneBookId = (dto.phoneBookId) ? dto.phoneBookId : null;
            this.name = (dto.name) ? dto.name : null;
        }
    }
}