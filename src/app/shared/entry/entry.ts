export class Entry {
    
    entryId: number;
    name: string;
    phoneNumber: string;
    phoneBookId: number;

    constructor(dto? :any){
        if(dto) {
            this.entryId = (dto.entryId) ? dto.entryId : null;
            this.name = (dto.name) ? dto.name : null;
            this.phoneNumber = (dto.phoneNumber) ? dto.phoneNumber : null;
            this.phoneBookId = (dto.phoneBookId) ? dto.phoneBookId : null;
        }
    }
}