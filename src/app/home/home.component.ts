import { Component, OnInit, ViewChild } from '@angular/core';
import { PhoneBookService } from '../services/api/PhoneBook/phonebook.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  

  constructor(
    public matDialog: MatDialog,
    private router: Router,
    private PhoneBookService: PhoneBookService,
  ) { }

  items: any[] = [];

  

  ngOnInit() : void{
    this.fetchPhoneBooks();
  }

  fetchPhoneBooks()
  {
    return this.PhoneBookService.getAllPhoneBookData().subscribe(res => this.items = res);
  }
  
  goToEntrys(item: any)
  {
    this.router.navigateByUrl('/entries/'+item.phoneBookId);
  }
  addPhoneBook()
  {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";

    dialogConfig.data = {
      title: "Insert Phone Book Details",
      entry: false
    }
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig)
    modalDialog.afterClosed().subscribe(res => this.fetchPhoneBooks());
  }
}
