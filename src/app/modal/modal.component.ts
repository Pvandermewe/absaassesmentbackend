import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PhoneBookService } from '../services/api/PhoneBook/phonebook.service';
import { EntryService } from '../services/api/Entry/entry.service';
import { PhoneBook } from '../shared/phonebook/phonebook';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  name: string;
  phonenumber: string;

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: any,
    private phoneBookService: PhoneBookService,
    private entryService: EntryService
  ) {
    console.log(this.modalData);
  }

  ngOnInit() {}

  actionFunction() {
    if(this.modalData.entry)
    {
      this.entryService.saveEntry({name: this.name, phoneNumber: this.phonenumber, phoneBookId: this.modalData.phoneBookId}).subscribe();
    }
    else
    {
      this.phoneBookService.savePhoneBook({name: this.name}).subscribe();
    }
    this.closeModal();
  }

  closeModal() {
    this.dialogRef.close();
  }

}