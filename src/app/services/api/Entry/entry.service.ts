import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiEndpoint } from '../api-end-point.service';
import { environment } from 'src/environments/environment';
import { ApiHttpService } from '../api-http.service';
import { map } from 'rxjs/operators';
import { Entry } from '../../../shared/entry/entry';

@Injectable({ providedIn: 'root' })
export class EntryService {
  private apiEndpointHelper = new ApiEndpoint(environment.PhoneBookServiceSuffix);

  constructor(private apiHttpService: ApiHttpService) { }

  public getAllEntryData(phoneBookId: number): Observable<Entry[]> {
    const url = this.apiEndpointHelper
      .createUrl('Entry/GetAllEntriesByPhoneBook')
      .addRouteParam(phoneBookId.toString())
      .build();

    return this.apiHttpService.get<any>(url)
      .pipe(map((res) => res?.transferObjectList?.length ? res.transferObjectList.map(entry => new Entry(entry)) : [new Entry()]));
    }

    public saveEntry(entryData: Partial<Entry>) {
      const url = this.apiEndpointHelper.createUrl(`Entry/AddEntry`);
      entryData.phoneBookId = parseFloat(entryData.phoneBookId.toString());
      return this.apiHttpService.post<any>(url.build(), JSON.stringify(entryData));
    }
}