import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiEndpoint } from '../api-end-point.service';
import { environment } from 'src/environments/environment';
import { ApiHttpService } from '../api-http.service';
import { map } from 'rxjs/operators';
import { PhoneBook } from '../../../shared/phonebook/phonebook';

@Injectable({ providedIn: 'root' })
export class PhoneBookService {
  private apiEndpointHelper = new ApiEndpoint(environment.PhoneBookServiceSuffix);

  constructor(private apiHttpService: ApiHttpService) { }

  public getAllPhoneBookData(): Observable<PhoneBook[]> {
    const url = this.apiEndpointHelper
      .createUrl('PhoneBook/GetAllPhoneBooks')
      .build();

    return this.apiHttpService.get<any>(url)
      .pipe(map((res) => res?.transferObjectList?.length ? res.transferObjectList.map(phonebook => new PhoneBook(phonebook)) : [new PhoneBook()]));
    }

    public savePhoneBook(phonebookData: Partial<PhoneBook>) {
      const url = this.apiEndpointHelper.createUrl(`PhoneBook/AddPhoneBook`);
      return this.apiHttpService.post<any>(url.build(), JSON.stringify(phonebookData));
    }
}