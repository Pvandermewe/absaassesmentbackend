import { environment } from 'src/environments/environment';

export class UrlBuilder {
  private action: string;
  private baseRouteParams: string[] = [];
  private routeParams: string[] = [];
  private queryParams: string[] = [];

  private get baseUrl() {
    return this.isMockAPI && !environment.production
      ? `${environment.ApiMockURL}` : `${environment.ApiBaseURL}/${this.targetService}`;
  }

  constructor(private targetService: string, action: string, private isMockAPI = false) {
    this.action = `${action}`;
  }

  public build(): string {
    const action = this.action.length ? `/${this.action}` : ``;
    return `${this.baseUrl}${this.baseRouteParams.join('/')}${action}${this.routeParams.join('/')}${this.queryParams.join('&')}`;
  }

  public addQueryParam(key: string, value: string | number | boolean) {
    if (value !== undefined) {
      this.queryParams.push(this.queryParams.length > 0 ? `${key}=${value}` : `?${key}=${value}`);
    }
    return this;
  }

  public addBaseRouteParam(route: string, value?: string) {
    const path = (value) ? `${route}/${value}` : route;
    this.baseRouteParams.push(this.baseRouteParams.length > 0 ? path : `/${path}`);
    return this;
  }

  public addRouteParam(route: string, value?: string) {
    const path = (value) ? `${route}/${value}` : route;
    this.routeParams.push(this.routeParams.length > 0 ? path : `/${path}`);
    return this;
  }
}

export class ApiEndpoint {

  constructor(private targetService: string, private isMockAPI = false) { }

  public createUrl(action?: string): UrlBuilder {
    return new UrlBuilder(this.targetService, action || '', this.isMockAPI);
  }
}
