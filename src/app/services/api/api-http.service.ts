// Angular Modules
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface RequestOptions {
  headers?: HttpHeaders;
  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export class ApiHttpService {

  constructor(
    private http: HttpClient
  ) { }

  public get<T>(url: string, options: RequestOptions = {headers: new HttpHeaders({'Access-Control-Allow-Origin':'*'})}): Observable<T> {
    return this.http.get<T>(url, options);
  }
  
  public post<TEntityResponse>(url: string, data: any, options: RequestOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})}) {
    return this.http.post<TEntityResponse>(url, data, options);
  }
  public put<TEntityResponse>(url: string, data: any, options: RequestOptions = {}) {
    return this.http.put<TEntityResponse>(url, data, options);
  }
  public delete<TEntityResponse>(url: string, options: RequestOptions = {}) {
    return this.http.delete<TEntityResponse>(url, options);
  }
}