import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { EntryService } from '../services/api/Entry/entry.service';
import { Entry } from '../shared/entry/entry';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.css']
})
export class EntriesComponent implements OnInit {

  id: number;

  constructor(
    public matDialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private entryService: EntryService,
  ) { }

  items: any[] = [];

  

  ngOnInit() : void{
    this.route.params.subscribe( params =>
      this.id = params['id']
    );
    this.fetchEntries();
  }

  fetchEntries()
  {
    return this.entryService.getAllEntryData(this.id).subscribe(res => this.items = res);
  }
  
  goToEntrys(item: any)
  {
    this.router.navigateByUrl('/entries/'+item.phoneBookId);
  }
  addEntry()
  {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";

    dialogConfig.data = {
      title: "Insert Phone Book Details",
      entry: true,
      phoneBookId: this.id
    }
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig)
    modalDialog.afterClosed().subscribe(res => this.fetchEntries());
  }
}
